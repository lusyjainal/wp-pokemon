import React from 'react'
import RoutesComponent from './routes/Routes'
import { createRoot } from 'react-dom/client'
import './index.css'

const container = document.getElementById('root')
const root = createRoot(container)

root.render(
  <React.StrictMode>
    <RoutesComponent />
  </React.StrictMode>
)
