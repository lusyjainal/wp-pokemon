/* eslint-disable no-undef */

/**
 * Codesandbox executes this setup file in a browser environment.
 * Ensure that the server-side API mocking runs in NodeJS only.
 * @note You DON'T need this condition in your test setup.
 */

import '@testing-library/jest-dom'
import { server } from './mocks/server'

// Enable API mocking before tests.
beforeAll(() => server.listen({ onUnhandledRequest: 'error' }))

// Reset any runtime request handlers we may add during the tests.
afterEach(() => server.resetHandlers())

// Disable API mocking after the tests are done.
afterAll(() => server.close())
