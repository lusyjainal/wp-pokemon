import { rest } from 'msw'
import { mockDataPokemon } from './index'

export const handlers = [
  /**
   * Example of a request handler—function that captures a request
   * and declares which mocked response to return upon match.
   * @see https://mswjs.io/docs/basics/request-handler
   */
  rest.get('https://pokeapi.co/api/v2/pokemon/1', (req, res, ctx) => {
    // console.log('----- res 2 ----')
    // console.log(res(ctx.status(200), ctx.json(mockDataPokemon)))
    return res(
      ctx.status(200),
      ctx.json(mockDataPokemon)
    )
  })
]
