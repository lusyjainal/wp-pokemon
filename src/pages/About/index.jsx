import React from 'react'
import Banner from '../../assets/pp-jainal.jpg'

const About = () => {
  return (
    <div className='flex justify-center'>
      <div className='rounded-md text-center shadow-sm max-w-md bg-white/40 backdrop-blur-xl overflow-auto mt-10'>
        <header className='mb-4'>
          <img src={Banner} alt='pp' className='w-full object-cover mx-auto' />
        </header>
        <section className='p-4'>
          <h1 className='text-slate-900 font-semibold text-xl mb-4'>Hi there 👋, my name is Jainal Arifin</h1>
        </section>
      </div>
    </div>
  )
}

export default About
