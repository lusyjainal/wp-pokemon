import React from 'react'
import { Card } from '../../components/ui'

const Home = () => {
  return (
    <div className='lg:grid lg:grid-cols-2'>
      <div className='text-center mt-10 lg:mt-32 lg:ml-20'>
        <h1 data-testid='home-title' className='text-white text-6xl'>WP<span className='text-red-500'>poke</span></h1>
        <p data-testid='home-desc' className='text-white p-4'>WPpoke (warung pintar pokemon) is a frontend application that connects to a free API called PokeApi, and facilitates the visualization and search of pokemon through a minimalist interface. This application was developed with the aim of putting into practice React concepts such as the use of Hooks, Custom Hooks, Context and Routes. In addition, the use of new technologies: Tailwinds as CSS Framework and Vite as a file packer, an alternative to WebPack.</p>
      </div>
      <div>
        <Card id='1' />
      </div>
    </div>
  )
}

export default Home
