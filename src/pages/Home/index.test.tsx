import React from "react";
import { describe, expect, it } from "vitest";
import Home from "./index";
import { render, screen } from "@testing-library/react";

describe("Home working test", () => {
  it("Should render title is visible", () => {
    render(<Home />);
    const title = screen.getByTestId("home-title");
    expect(title.textContent).toBe("WPpoke");
    // console.log('---2 --');
    // console.log(screen.debug());
    
    
  });

  it("Should render desc is visible", () => {
    render(<Home />);
    const desc = screen.getByTestId("home-desc");
    expect(desc.textContent).toBe(
      "WPpoke (warung pintar pokemon) is a frontend application that connects to a free API called PokeApi, and facilitates the visualization and search of pokemon through a minimalist interface. This application was developed with the aim of putting into practice React concepts such as the use of Hooks, Custom Hooks, Context and Routes. In addition, the use of new technologies: Tailwinds as CSS Framework and Vite as a file packer, an alternative to WebPack."
    );
  });
});
