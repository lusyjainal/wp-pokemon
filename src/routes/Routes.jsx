import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import App from '../App'
import { Home, Compare, About } from '../pages'

const RoutesComponent = () => {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<App />}>
          <Route index element={<Home />} />
          <Route path='compare' element={<Compare />} />
          <Route path='about' element={<About />} />
        </Route>
      </Routes>
    </Router>
  )
}

export default RoutesComponent
