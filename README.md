
# link demo web
https://jainal-pokemon.netlify.app/

### cara menjalankan

- masuk ke direktori
- jalankan perintah **npm install**
- ketika sudah jalankan perintah **npm run dev**

### cara testing

- masuk ke direktori
- jalankan perintah **npm run test**

### cara melihat coverage

- masuk ke direktori
- jalankan perintah **npm run coverage**
